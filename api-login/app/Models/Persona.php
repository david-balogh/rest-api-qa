<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Persona extends Model
{
    protected $id;
    protected $dni;
    protected $nombre;
    protected $apellido;
    protected $direccion;
    protected $username;
    protected $password;
    protected $legajo;

    function __construct($username) {
        $person = app('db')->select("SELECT * FROM person where username='".$username."' limit 1");
        if(sizeof($person) == 1){
            $this->id = $person[0]->id;
            $this->dni = $person[0]->dni;
            $this->nombre = $person[0]->nombre;
            $this->apellido = $person[0]->apellido;
            $this->direccion = $person[0]->direccion;
            $this->username = $person[0]->username;
            $this->password = $person[0]->password;
            $this->legajo = $person[0]->legajo;
        }
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of dni
     */ 
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of apellido
     */ 
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Get the value of direccion
     */ 
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Get the value of username
     */ 
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the value of legajo
     */ 
    public function getLegajo()
    {
        return $this->legajo;
    }
}