<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use \Illuminate\Support\Facades\Hash;


class CoreFacade extends BaseController
{

    public function maquinas(Request $request)
    {
		
		$jsonRequest = json_decode($request->getContent());

        try{
            $machines = new \App\Models\Machines($request->person_id);
            $machines->cargarContenido();
            $status = 200;
        }
        catch(Exception $e){
            $machines = new \App\Models\Machines($request->person_id);
            $machines->error = true;
            $machines->error_description = 'select_failure';
            $status = 500;
        }
		
        
        $content = $machines->toJson();
        
        return (new Response($content, $status))
				  ->header('Content-Type', 'application/json');
				  
    }

    public function dispositivos(Request $request)
    {
		
		$jsonRequest = json_decode($request->getContent());

        try{
            $devices = new \App\Models\Devices($request->person_id);
            $devices->setMachineId($request->machine_id);
            $devices->cargarContenido();
            $status = 200;
        }
        catch(Exception $e){
            $devices = new \App\Models\Devices($request->person_id);
            $devices->error = true;
            $devices->error_description = 'select_failure';
            $status = 500;
        }
		
        
        $content = $devices->toJson();
        
        return (new Response($content, $status))
				  ->header('Content-Type', 'application/json');
				  
    }

    public function persona(Request $request)
    {
		
		$jsonRequest = json_decode($request->getContent());

        try{
            $person = new \App\Models\Person($request->person_id);
            $person->cargarContenido();
            $status = 200;
        }
        catch(Exception $e){
            $person = new \App\Models\Person($request->person_id);
            $person->error = true;
            $person->error_description = 'select_failure';
            $status = 500;
        }
		
        
        $content = $person->toJson();
        
        return (new Response($content, $status))
				  ->header('Content-Type', 'application/json');
				  
    }
}