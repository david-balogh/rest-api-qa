<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

#Commands:
# php artisan make:migration create_person_table
# php artisan migrate --force
class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person', function (Blueprint $table) {
            $table->id();
            $table->text('dni');
            $table->text('nombre');
            $table->text('apellido');
            $table->text('direccion');
            $table->text('username');
            $table->text('password');
            $table->text('legajo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person');
    }
}
