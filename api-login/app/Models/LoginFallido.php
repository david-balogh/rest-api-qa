<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoginFallido
{
    public $error;
    public $error_description;

    function __construct() {
        $this->error = true;
        $this->error_description = 'invalid_grant';
    }

    public function toJson($options = 0){
        return json_encode($this);
    }

    public function getStatusCode(){
        return 400;
    }
}