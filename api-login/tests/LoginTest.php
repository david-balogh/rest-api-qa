<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    public function testLoginCorrecto()
    {
        $this->json('POST', '/login', ['username' => 'dave', 'password' => '123'])
             ->seeJson([
                'error' => false,
             ]);
    }

    public function testLoginPasswordInorrecta()
    {
        $this->json('POST', '/login', ['username' => 'dave', 'password' => '1234'])
             ->seeJson([
                'error' => true,
             ]);
    }

    public function testLoginUsuarioInorrecto()
    {
        $this->json('POST', '/login', ['username' => 'inexistente', 'password' => '123'])
             ->seeJson([
                'error' => true,
             ]);
    }
}
